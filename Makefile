
YUI_COMPRESSOR_VERSION=2.4.8

all: dist/mini-promise.js dist/mini-promise.min.js

install: all
	npm link

node_modules: package.json package-lock.json
	npm ci

build/Prom.js: node_modules src/*
	rm -rf build
	$(shell npm bin)/tsc

yuicompressor.jar:
	curl -L https://github.com/yui/yuicompressor/releases/download/v${YUI_COMPRESSOR_VERSION}/yuicompressor-${YUI_COMPRESSOR_VERSION}.jar -o ./yuicompressor.jar

dist:	build/Prom.js
	rm -rf dist
	mkdir dist

dist/mini-promise.min.js: dist build/Prom.js yuicompressor.jar
	java -jar ./yuicompressor.jar --type js build/Prom.js -o dist/mini-promise.min.js

dist/mini-promise.js: dist build/Prom.js
	cp ./build/Prom.js ./dist/mini-promise.js
