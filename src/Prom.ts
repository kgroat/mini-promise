
type PromCallback<T> = (resolve: (val?: T | Prom<T>) => void, reject: (err?: any) => void) => void

type PromThen<V, R> = (val: V) => R | Prom<R>

type PromResolution = 'pending' | 'resolved' | 'rejected'

function isPromise<T> (candidate: any): candidate is Prom<T> {
  return candidate instanceof Prom || (typeof candidate.then === 'function' && typeof candidate['catch'] === 'function')
}

class Prom<T> implements Promise<T> {
  static resolve = <T>(val: T | Prom<T>) => new Prom<T>(resolve => resolve(val))
  static reject = <T = void>(err: any) => new Prom<T>((_, reject) => reject(err))

  private _resolveHandlers: PromThen<T, any>[] = []
  private _rejectHandlers: PromThen<any, any>[] = []
  private _finallyHandlers: (() => void)[] = []
  private _resolution: PromResolution = 'pending'
  private _value?: T
  private _error?: any
  readonly [Symbol.toStringTag] = 'Promise'

  constructor (cb: PromCallback<T>) {
    const _reject = (err?: any) => {
      setTimeout(() => {
        if (this._resolution !== 'pending') { return }
        this._resolution = 'rejected'
        this._error = err
        if (this._rejectHandlers.length === 0) {
          console.error('Unhandled promise rejection:', err)
        } else {
          this._rejectHandlers.forEach(h => h(err))
        }
        this._finallyHandlers.forEach(h => h())
        this._resolveHandlers = []
        this._rejectHandlers = []
        this._finallyHandlers = []
      }, 0)
    }

    const _onResolved = (val?: T) => {
      setTimeout(() => {
        if (this._resolution !== 'pending') { return }
        this._resolution = 'resolved'
        this._value = val
        this._resolveHandlers.forEach(h => h(val!))
        this._finallyHandlers.forEach(h => h())
        this._resolveHandlers = []
        this._rejectHandlers = []
        this._finallyHandlers = []
      }, 0)
    }

    const _resolve = (val?: T | Prom<T>) => {
      if (this._resolution !== 'pending') { return }
      if (isPromise<T>(val)) {
        val.then(_onResolved)['catch'](_reject)
      } else {
        _onResolved(val)
      }
    }

    setTimeout(() => {
      try {
        cb(_resolve, _reject)
      } catch (e) {
        _reject(e)
      }
    }, 0)
  }

  static all<T> (...promises: Promise<T>[]): Promise<T[]> {
    return new Promise((resolve, reject) => {
      const results: T[] = []
      let doneCount = 0
      promises.forEach((each, idx) => {
        each.then(result => {
          results[idx] = result
          if (++doneCount === promises.length) {
            resolve(results)
          }
        })['catch'](reject)
      })
    })
  }

  static race<T> (...promises: Promise<T>[]): Promise<T> {
    return new Promise((resolve, reject) => {
      promises.forEach((each) => {
        each.then(resolve)['catch'](reject)
      })
    })
  }

  then<R1 = T, R2 = never> (): Prom<R1>
  then<R1 = T, R2 = never> (onResolve: PromThen<T, R1> | null): Prom<R1>
  then<R1 = T, R2 = never> (onResolve: PromThen<T, R1> | null, onReject: PromThen<any, R2> | null): Prom<R1 | R2>
  then<R1 = T, R2 = never> (onResolve?: PromThen<T, R1> | null, onReject?: PromThen<any, R2> | null): Prom<R1 | R2> {
    const self = this as any as Prom<R1 | R2>
    if (this._resolution === 'pending') {
      if (!onResolve && !onReject) {
        return self
      }
      return new Prom<R1 | R2>((resolve, reject) => {
        if (onResolve) {
          this._resolveHandlers.push(val => {
            try {
              resolve(onResolve(val) as R1)
            } catch (e) {
              reject(e)
            }
          })
        }
        if (onReject) {
          this._rejectHandlers.push(val => {
            try {
              resolve(onReject(val) as R2)
            } catch (e) {
              reject(e)
            }
          })
        } else {
          this._rejectHandlers.push(reject)
        }
      })
    } else if (this._resolution === 'resolved') {
      if (onResolve) {
        try {
          return Prom.resolve<R1 | R2>(onResolve(this._value!) as R1)
        } catch (e) {
          return Prom.reject<R1 | R2>(e)
        }
      } else {
        return self
      }
    } else if (this._resolution === 'rejected') {
      if (onReject) {
        try {
          return Prom.resolve<R1 | R2>(onReject(this._value!) as R2)
        } catch (e) {
          return Prom.reject<R1 | R2>(e)
        }
      } else {
        return self
      }
    } else {
      return neverPromise
    }
  }

  ['catch'] = <R = T>(onReject?: PromThen<any, R>): Prom<R> => {
    const self = this as any as Prom<R>
    if (this._resolution === 'pending') {
      if (!onReject) {
        return self
      } else {
        return new Prom<R>((resolve, reject) => {
          this._rejectHandlers.push(err => {
            try {
              resolve(onReject(err))
            } catch (e) {
              reject(e)
            }
          })
        })
      }
    } else if (this._resolution === 'rejected') {
      if (!onReject) {
        return self
      } else {
        try {
          const result = onReject(this._error)
          return Prom.resolve(result)
        } catch (e) {
          return Prom.reject(e)
        }
      }
    } else {
      return neverPromise
    }
  }

  ['finally'] = (onFinally?: () => void | null): Prom<T> => {
    return new Prom<T>((resolve, reject) => {
      this._finallyHandlers.push(() => {
        if (onFinally) {
          try {
            onFinally()
            resolve(this)
          } catch (e) {
            reject(e)
          }
        } else {
          resolve(this)
        }
      })
    })
  }
}

const neverPromise = new Prom<any>(() => {})
neverPromise.then = (() => {}) as any
neverPromise['catch'] = (() => {}) as any
neverPromise['finally'] = (() => {}) as any
